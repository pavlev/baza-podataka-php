<?php 
include('server.php'); 

if (!isset($_SESSION['Korisnicko_ime'])) {
  	$_SESSION['msg'] = "Prvo se morate prijaviti";
  	header('location: prijava.php');
  }

  if (isset($_GET['odjavi'])) {
  	session_destroy();
  	unset($_SESSION['Korisnicko_ime']);
  	header("location: prijava.php");
  }
?>
<!doctype html>
<head>
<meta charset="UTF-8"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="favicon.ico"> 
<link rel="stylesheet" href="Navbar_reg.css">
<link rel="stylesheet" href="registracija_prijava.css">
</head>
<body>
<title> Android blog </title>

<div class="navbar">
   <label for="meni" class="meni">Otvori meni</label>
    <input type="checkbox" id="meni" role="button">
        <ul id="meni">
        <li><a href="glavna.php">Naslovna</a></li>
        <li><a href="registracija.php">Registracija</a></li>
        <li><a href="pretraga.php">Pretraga</a></li>
        <li><a href="unos.php">Unos </a></li>
        <li><a href="izmena.php">Izmena </a></li>
        <li><a href="brisanje.php">Brisanje </a></li>
        <li><a href="upload.php">Upload </a></li>
         <li><a href="join.php">Sve tabele </a></li>
    </ul>
	</div>

<div class="bg">
</div> 
<center>
<div class="reg">
<h1>Početna stranica  </h1>
<div class="poruka">

<?php if (isset($_SESSION['uspeh'])) : ?>
      <div >
      	<h3>
          <?php 
          	echo $_SESSION['uspeh']; 
          	unset($_SESSION['uspeh']);
          ?>
          </h3>
      </div>
  <?php endif ?>
<?php  if (isset($_SESSION['Korisnicko_ime'])) : ?>
      <p>Dobrodošli <strong><?php echo $_SESSION['Korisnicko_ime']; ?></strong></p>
      <p> <a href="glavna.php?odjavi='1'" style="color: orange;">Odjavite se</a> </p>
    <?php endif ?>
</div>
</form>
</div>
</body>
</html>