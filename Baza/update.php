<?php 

$IMEI="";
$proizvodjac="";
$model="";

$greske = array(); 

include('konekcija.php'); 

$konekcija = OstvariKonekciju();

if (isset($_POST['dugme_update'])) { //provera da li je dugme stisnuto
  // receive all input values from the form
  $IMEI=mysqli_real_escape_string($konekcija, $_POST['IMEI']);
  $proizvodjac=mysqli_real_escape_string($konekcija, $_POST['proizvodjac']);
  $model=mysqli_real_escape_string($konekcija, $_POST['model']);
 

 if (empty($IMEI)) { array_push($greske, "IMEI je obavezan"); }
  if (empty($proizvodjac)) { array_push($greske, "Naziv proizvođača je obavezan"); }
  if (empty($model)) { array_push($greske, "Naziv modela je obavezan"); }
 

  $provera = "SELECT * FROM mobilni_telefon WHERE IMEI='$IMEI' LIMIT 1";
  $rezultat = mysqli_query($konekcija, $provera);
  $telefon = mysqli_fetch_assoc($rezultat);

  if ($telefon) { // if user exists
    if ($telefon['IMEI'] != $IMEI) {
      array_push($greske, "Telefon ne postoji");
    }
}
if (count($greske) == 0) {
  	$unos = "UPDATE mobilni_telefon SET Proizvodjac='$proizvodjac', Model='$model'  WHERE IMEI='$IMEI'";
  	mysqli_query($konekcija, $unos);
  }
}
?>
<!doctype html>
<head>
<meta charset="UTF-8"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="favicon.ico"> 
<link rel="stylesheet" href="Navbar_reg.css">
<link rel="stylesheet" href="registracija.css">
</head>
<body>
<title> Android blog </title>

<div class="navbar">
   <label for="meni" class="meni">Otvori meni</label>
    <input type="checkbox" id="meni" role="button">
        <ul id="meni">
        <li><a href="glavna.php">Naslovna</a></li>
        <li><a href="registracija.php">Registracija</a></li>
        <li><a href="pretraga.php">Pretraga</a></li>
        <li><a href="unos.php">Unos </a></li>
        <li><a href="izmena.php">Izmena </a></li>
        <li><a href="delete.php">Brisanje </a></li>
    </ul>
	</div>

<div class="bg">
</div> 
<center>
<div class="reg">
<h1> Izmenite podatke mobilnog telefona </h1>
<form name="registracija" class="regi" method="post" action="izmena.php" >

<p> Unesite IMEI:  <br><input type="text" size="14" name="IMEI" value="<?php echo $IMEI;?>"> </p>

<p> Izmenite proizvođača: <br><input type="text" size="14" name="proizvodjac" value="<?php echo $proizvodjac;?>"> </p>

<p> Izmenite model: <br><input type="text" size="15" name="model" value="<?php echo $model;?>" > </p>
<p> <input type="submit" value="Prosledite" name="dugme_update"> </p>
<div class="greske">	<?php include ('greske.php'); ?></div>

</form>
</div>
</body>
</html>