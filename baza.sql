-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 23, 2018 at 09:48 PM
-- Server version: 5.7.21
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `baza`
--

-- --------------------------------------------------------

--
-- Table structure for table `deo`
--

DROP TABLE IF EXISTS `deo`;
CREATE TABLE IF NOT EXISTS `deo` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Naziv` varchar(15) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=132458 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deo`
--

INSERT INTO `deo` (`ID`, `Naziv`) VALUES
(132456, 'Modul kamere'),
(132457, 'Baterija');

-- --------------------------------------------------------

--
-- Table structure for table `korisnik`
--

DROP TABLE IF EXISTS `korisnik`;
CREATE TABLE IF NOT EXISTS `korisnik` (
  `JMBG` int(13) NOT NULL AUTO_INCREMENT,
  `Korisnicko_ime` varchar(12) NOT NULL,
  `Lozinka` varchar(12) NOT NULL,
  `email` varchar(15) NOT NULL,
  PRIMARY KEY (`JMBG`)
) ENGINE=InnoDB AUTO_INCREMENT=1232141213 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `korisnik`
--

INSERT INTO `korisnik` (`JMBG`, `Korisnicko_ime`, `Lozinka`, `email`) VALUES
(1234, 'Bojan', '1234', 'bojan@gmail.com'),
(12345, 'Nenad', '1234', 'nenad@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `mobilni_telefon`
--

DROP TABLE IF EXISTS `mobilni_telefon`;
CREATE TABLE IF NOT EXISTS `mobilni_telefon` (
  `IMEI` int(11) NOT NULL AUTO_INCREMENT,
  `Proizvodjac` varchar(11) NOT NULL,
  `Model` varchar(11) NOT NULL,
  `JMBG_korisnika` int(13) DEFAULT NULL,
  PRIMARY KEY (`IMEI`),
  UNIQUE KEY `JMBG_korisnik` (`JMBG_korisnika`)
) ENGINE=InnoDB AUTO_INCREMENT=214216522 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobilni_telefon`
--

INSERT INTO `mobilni_telefon` (`IMEI`, `Proizvodjac`, `Model`, `JMBG_korisnika`) VALUES
(21412, 'Sony', 'V30', NULL),
(123456, 'Huawei', 'M10', 12345),
(12321412, 'Samsung', 'S9', 1234),
(12345678, 'Nemam', 'pojma', NULL),
(214216521, 'Svemir', 'Astr', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `poseduje`
--

DROP TABLE IF EXISTS `poseduje`;
CREATE TABLE IF NOT EXISTS `poseduje` (
  `IMEI_telefona` int(11) NOT NULL,
  `ID_dela` int(11) NOT NULL,
  PRIMARY KEY (`IMEI_telefona`,`ID_dela`),
  UNIQUE KEY `IMEI_telefona` (`IMEI_telefona`,`ID_dela`),
  KEY `ID_dela` (`ID_dela`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poseduje`
--

INSERT INTO `poseduje` (`IMEI_telefona`, `ID_dela`) VALUES
(12321412, 132456);

-- --------------------------------------------------------

--
-- Table structure for table `servis`
--

DROP TABLE IF EXISTS `servis`;
CREATE TABLE IF NOT EXISTS `servis` (
  `ID_s` int(10) NOT NULL AUTO_INCREMENT,
  `Naziv_s` varchar(10) NOT NULL,
  `IMEI_telefona` int(11) NOT NULL,
  PRIMARY KEY (`ID_s`),
  UNIQUE KEY `IMEI_telefona` (`IMEI_telefona`)
) ENGINE=InnoDB AUTO_INCREMENT=98322 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `servis`
--

INSERT INTO `servis` (`ID_s`, `Naziv_s`, `IMEI_telefona`) VALUES
(9877, 'Begram', 12321412);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `mobilni_telefon`
--
ALTER TABLE `mobilni_telefon`
  ADD CONSTRAINT `mobilni_telefon_ibfk_1` FOREIGN KEY (`JMBG_korisnika`) REFERENCES `korisnik` (`JMBG`);

--
-- Constraints for table `poseduje`
--
ALTER TABLE `poseduje`
  ADD CONSTRAINT `poseduje_ibfk_1` FOREIGN KEY (`ID_dela`) REFERENCES `deo` (`ID`),
  ADD CONSTRAINT `poseduje_ibfk_2` FOREIGN KEY (`IMEI_telefona`) REFERENCES `mobilni_telefon` (`IMEI`);

--
-- Constraints for table `servis`
--
ALTER TABLE `servis`
  ADD CONSTRAINT `servis_ibfk_1` FOREIGN KEY (`IMEI_telefona`) REFERENCES `mobilni_telefon` (`IMEI`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
